# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is EdAcc? ###

*EdAcc* is a voice control script for *Elite: Dangerous* (http://elitedangerous.com), written in Python.

## Installation ##

Unpack your downloaded archive in your desired folder.

### Requirements ###

* windows xp and above
* windows **or** Nuance Dragon (http://www.nuance.com/dragon/index.htm) speech recognition configured and working 
* text-to-speech configured and working
* [python 2.7](http://python.org) 

### Python modules required ###

* [lxml](http://www.lfd.uci.edu/~gohlke/pythonlibs/#lxml)
* [cssselect](https://pypi.python.org/pypi/cssselect)
* [dragonfly](https://github.com/markevans/dragonfly)
* [pywin32](http://sourceforge.net/projects/pywin32)
* [sqlalchemy](https://pypi.python.org/pypi/SQLAlchemy)

## Configuration ##

Configuration file `config.json` is located in the root folder of application.

## Usage ##

run ** python astra.py **