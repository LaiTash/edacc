import os
from traceback import print_exc
import time

import pythoncom
from dragonfly import Grammar
from dragonfly.all import CompoundRule
from sqlalchemy.orm import sessionmaker

from acc.database import create_db, db_base


class ComputerError(Exception):
    pass


class Computer(object):
    def __init__(self, config):
        self.jobs = {}
        self.running = False
        self.grammar = Grammar('ED ACC SR GRAMMAR')
        self.config = config
        sql_path = os.path.join(config.get('savefiles_folder', './'),
                                config.get('db_filename', 'edacc.db'))
        self.db_engine = create_db('/%s' % self.config.get('db_filename',
                                                           'edacc.db'))
        self.db_session = sessionmaker(self.db_engine)()
        db_base.bind = self.db_engine
        db_base.metadata.bind = self.db_engine
        db_base.metadata.create_all()

    def init_subsystems(self, subsystems):
        self.io = subsystems['output']
        self.glob = subsystems['glob']
        self.add_job(self.glob)
        self.add_job(self.io)

    def stop(self):
        self.running = False
        for job in self.jobs.itervalues():
            job.stop()

    def sleep(self, s):
        time.sleep(s)

    def log(self, message):
        print(message)

    def add_job(self, job):
        name = job.name()
        self.log('ADDING %s' % job.name())
        if name in self.jobs:
            raise ComputerError('Job "%s" already exists.' % name)
        self.jobs[name] = job

    def run_jobs(self):
        for job in self.jobs.itervalues():
            try:
                job.job()
            except Exception as e:
                print print_exc(e)

    def get_job(self, cls):
        if isinstance(cls, str):
            return next((job for job in self.jobs.itervalues()
                         if job.__class__.__name__ == cls), None)
        return next((job for job in self.jobs.itervalues()
                     if isinstance(job, cls)), None)

    def run(self):
        for job in self.jobs.itervalues():
            job.start()
        for job in self.jobs.itervalues():
            job.launch()
        self.running = True
        self.grammar.load()
        self.io.notify('Input subsystem online.')
        self.io.notify('[Welcome|hello], {commander}.')
        while self.running:
            pythoncom.PumpWaitingMessages()
            self.run_jobs()
            self.sleep(.1)
            self.db_session.commit()

class CommandRule(CompoundRule):
    def __init__(self, computer, *args, **kwargs):
        self.computer = computer
        super(CommandRule, self).__init__(*args, **kwargs)

