import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base


def create_db(dbfile):
    engine = sqlalchemy.create_engine('sqlite://%s' % dbfile)
    return engine


db_base = declarative_base()
