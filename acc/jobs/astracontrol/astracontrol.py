import uuid
import time

from acc.jobs.astracontrol.rules import AstraShutdown, CancelCommand
from acc.jobs.job import ComputerJob


class UndoCommand(object):
    def __init__(self, name, undo, uid, position):
        self.uid = uid
        self.position = position
        self.name = name
        self.undo_callback = undo

    def undo(self):
        self.undo_callback()


class YesNoControl(object):
    def __init__(self):
        self.on_yes = None
        self.on_no = None
        self.wait_until = None

    def ask(self, yes, no, wait=20):
        self.on_yes = yes
        self.on_no = no
        self.wait_until = time.time() + wait

    def awaiting_confirmation(self):
        return self.wait_until and self.wait_until < time.time()

    def clear(self):
        self.on_no = None
        self.on_yes = None
        self.wait_until = None

    def yes(self):
        self.on_yes()
        self.clear()

    def no(self):
        self.on_no()
        self.clear()


class AstracontrolJob(ComputerJob):
    def name(self):
        return "AstraControl"

    def start(self):
        self.yes_no_dialog = YesNoControl()
        self.command_history = {}
        self.computer.grammar.add_rule(AstraShutdown(self.computer))
        self.computer.grammar.add_rule(CancelCommand(self.computer))

    def shutdown(self):
        self.computer.stop()

    def last_history(self):
        if self.command_history:
            return max(self.command_history.values(), key=lambda c: c.position)
        else:
            return None

    def add_history(self, name, undo, uid=None):
        if not uid:
            uid = uuid.uuid1()
        last_command = self.last_history()
        last_position = last_command.position if last_command else -1
        self.command_history[uid] = (
            UndoCommand(name, undo, uid, last_position + 1))
        return uid

    def remove_history(self, uid):
        self.command_history.pop(uid, None)

    def undo_last(self):
        if self.command_history:
            last_command = self.last_history()
            self.command_history.pop(last_command.uid)
            last_command.undo()
            return last_command
        else:
            self.computer.io.notify('There is nothing to undo, {commander}')