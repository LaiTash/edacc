from acc.computer import CommandRule


class CancelCommand(CommandRule):
    spec = '(Cancel last command|void that)'

    def _process_recognition(self, node, extras):
        job = self.computer.get_job('AstracontrolJob')
        last_command = job.undo_last()
        if last_command:
            self.computer.io.notify('Deleting command: %s' % last_command.name)


class AstraShutdown(CommandRule):
    spec = 'Shut down Astra'

    def _process_recognition(self, node, extras):
        self.computer.io.notify('Shutting down, [{commander}|]')
        self.computer.stop()
