import time

from acc.jobs.chitchat.rules import GratitudeRule, _AstraRule
from acc.jobs.job import ComputerJob


class ChitchatJob(ComputerJob):
    def name(self):
        return "Chitchat"

    def start(self):
        self.awaiting_gratitude = None
        self.computer.grammar.add_rule(GratitudeRule(self.computer))
        self.computer.grammar.add_rule(_AstraRule(self.computer))

    def expect_gratitude(self, t=15000):
        self.awaiting_gratitude = time.time() + t

    def gratitude_received(self):
        print(self.awaiting_gratitude, time.time())
        if self.awaiting_gratitude and self.awaiting_gratitude > time.time():
            t = 'You are welcome, [{commander}|]'
            self.computer.io.notify(t)
        else:
            variations = "[gratitude is not required" \
                         "|you have nothing to thank me for], {commander}"
            self.computer.io.notify(variations)
        self.awaiting_gratitude = None