from acc.computer import CommandRule


class GratitudeRule(CommandRule):
    name = 'gratitude'
    spec = "(thank you|thanks) [astra]"

    def _process_recognition(self, node, extras):
        job = self.computer.get_job('ChitchatJob')
        if job:
            job.gratitude_received()


class _AstraRule(CommandRule):
    spec = "Astra"

    def _process_recognition(self, node, extras):
        self.computer.io.notify('Yes[ {commander}|]?')


