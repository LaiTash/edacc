from time import time

from acc.jobs.job import ComputerJob, JobRule


class _YesRule(JobRule):
    spec = '(yes|do it)[ please][ astra]'

    def _process_recognition(self, node, extras):
        if self.job.awaiting_response():
            self.job.respond_yes()
        else:
            self.job.computer.io.notify('Please Elaborate {commander}')


class _NoRule(JobRule):
    spec = '(no|no need)[ please][ astra]'

    def _process_recognition(self, node, extras):
        if self.job.awaiting_response():
            self.job.respond_no()
        else:
            self.job.computer.io.notify('Please Elaborate {commander}')


class YesNoJob(ComputerJob):
    def name(self):
        return "dialog_YesNo"

    def start(self):
        self.yes_cb = None
        self.no_cb = None
        self.wait_until = None
        self.computer.grammar.add_rule(_YesRule(self))
        self.computer.grammar.add_rule(_NoRule(self))

    def init(self, yes, no, timeout=20, expiry=None):
        self.yes_cb = yes
        self.no_cb = no
        self.expiry = expiry
        self.wait_until = time() + timeout if timeout else None

    def awaiting_response(self):
        return self.wait_until and self.wait_until > time()

    def reset(self):
        self.init(None, None, None, None)

    def respond_yes(self):
        if self.yes_cb:
            self.yes_cb()
        self.reset()

    def respond_no(self):
        if self.no_cb:
            self.no_cb()
        self.reset()

    def job(self):
        if self.wait_until and self.wait_until < time():
            if self.expiry:
                self.expiry()
            self.reset()