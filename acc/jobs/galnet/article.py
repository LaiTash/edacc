from sqlalchemy import Column, DateTime, Integer, String, Text, Boolean

from acc.database import db_base


class Article(db_base):
    __tablename__ = 'articles'

    id_ = Column(Integer, primary_key=True, autoincrement=True)
    uid = Column(String, unique=True)
    url = Column(String, unique=True)
    date = Column(DateTime, nullable=True)
    title = Column(String)
    body = Column(Text)
    read = Column(Boolean, default=False)

    def date_str(self):
        return self.date.strftime('%d %B, %Y') if self.date else 'No date'

    def title_str(self):
        return self.title or 'No title'

    def body_str(self):
        return self.body or ''

