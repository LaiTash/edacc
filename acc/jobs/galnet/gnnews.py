from itertools import ifilter
import time
import urllib2
import datetime
import re

from lxml import html

from acc.jobs.dialogs.yesno.job import YesNoJob
from acc.jobs.galnet.article import Article
from acc.jobs.galnet.rules import _GetUnreadRule, _MarkFromToRead, _ReadArticle
from acc.jobs.job import ComputerJob


class GalnetJob(ComputerJob):
    def name(self):
        return "GalNetNews"

    post_date_regexp = re.compile('(?P<day>\d{1,2}) (?P<month>[A-Z]{3}) '
                                  '(?P<year>\d{4})')

    def __init__(self, *args, **kwargs):
        super(GalnetJob, self).__init__(*args, **kwargs)
        self.interval = self.computer.config.get('galnet_refresh_interval',
                                                 120)

    def set_next_check(self):
        self.next_check = time.time() + self.interval

    def start(self):
        self.set_next_check()
        self.computer.grammar.add_rule(_GetUnreadRule(self))
        self.computer.grammar.add_rule(_MarkFromToRead(self))
        self.computer.grammar.add_rule(_ReadArticle(self))

    # self.computer.grammar.add_rule(GnnewsReadLastRule(self.computer))

    def read_article(self, article):
        df = self.computer.config.get('verbose_date_format',
                                      '%d %B, %Y')
        self.computer.io.speak(article.date.strftime(df))
        self.computer.sleep(1)
        self.computer.io.speak(article.title)
        self.computer.sleep(1)
        self.computer.io.speak(article.body)
        self.computer.sleep(05)
        self.computer.io.speak('End of article.')
        article.read = True

    def read_title(self, article):
        self.computer.io.speak(article.id_)
        time.sleep(0.5)
        self.computer.io.speak(article.title)

    def read_titles(self, articles):
        def _wr():
            prev_date = None
            for article in sorted(articles,
                                  key=lambda a: (a.date, -a.id_),
                                  reverse=True):
                if prev_date != article.date:
                    df = self.computer.config.get('verbose_date_format',
                                                  '%d %B, %Y')
                    self.computer.io.speak(article.date.strftime(df))
                    prev_date = article.date
                    time.sleep(1)
                self.read_title(article)
                time.sleep(1)

        return _wr

    def notify_new_posts(self, posts):
        self.computer.io.notify('{commander}, there are some news on GalNet')
        gj = self.computer.get_job('ChitchatJob')
        if gj:
            gj.expect_gratitude()
        if YesNoJob.instance:
            self.computer.io.notify('Would you like me to read their '
                                    'titles[ {commander}|]?')
            YesNoJob.instance.init(self.read_titles(posts), None)

    def job(self):
        if time.time() < self.next_check:
            return
        new_posts = self.get_articles
        if new_posts:
            self.notify_new_posts(new_posts)
        self.set_next_check()

    def get_unread_articles(self, limit=0xffff):
        query = self.computer.db_session.query(Article).filter \
            (Article.read == False).limit(limit)
        return query.all()

    @property
    def get_articles(self):
        self.computer.log('Downloading GalNet news')
        current_time = time.time()
        db_session = self.computer.db_session
        page = self.download_articles()
        new_posts = []
        if page:
            root = html.parse(page).getroot()
            for article in root.cssselect('.galnet .article'):
                date_ = article.cssselect('.i_right .small')
                date_ = ifilter(None,
                                map(lambda el: self.post_date_regexp.search
                                (el.text.strip()), date_))
                date_ = next(date_, None)
                if date_:
                    date_ = datetime.datetime.strptime(
                        date_.group(), '%d %b %Y')
                url = article.cssselect('.hiLite a')
                title = url[0].text.strip() if url else None
                url = url[0].get('href') if url else None
                xpath = './*[@class="hiLite"]/following-sibling::p'
                body = '\n\n'.join(map(lambda e: e.text, article.xpath(xpath)))
                if db_session.query(Article).filter(
                                Article.uid == url).first():
                    continue
                article = Article(uid=url, url=url, date=date_, title=title,
                                  body=body)
                new_posts.append(article)
                db_session.add(article)
                db_session.commit()
        return new_posts

    def download_articles(self):
        try:
            return urllib2.urlopen(
                'http://www.elitedangerous.com/news/galnet/')
        except Exception as e:
            self.computer.log(e)