import time

from dragonfly import Number

from acc.computer import CommandRule
from acc.jobs.dialogs.yesno.job import YesNoJob
from acc.jobs.galnet.article import Article
from acc.jobs.job import JobRule


class _ReadArticle(JobRule):
    spec = '[Astra ][please ]read[ me] article[ number] <n>'
    extras = [
        Number('n')
    ]

    def _process_recognition(self, node, extras):
        n = extras.get('n')
        result = self.job.computer.db_session.query(Article) \
            .filter(Article.id_ == n).first()
        if result:
            self.job.read_article(result)
            cc = self.job.computer.jobs.get('Chitchat')
            if cc:
                cc.expect_gratitude()
        else:
            self.job.computer.io.notify(
                'There is no article numbered %i[ {commander}|]' % n)



class _MarkFromToRead(JobRule):
    spec = '[please ]mark articles [from <from> to <to>] as read[ astra]'
    extras = [
        Number('from'),
        Number('to')
    ]

    def _process_recognition(self, node, extras):
        from_ = extras.get('from')
        to_ = extras.get('to')
        articles = self.job.computer.db_session.query(Article) \
            .filter((Article.id_ >= from_) & (Article.id_ <= to_) &
                    (Article.read == False)).all()
        for article in articles:
            article.read = True
        self.job.computer.io.notify('%i articles marked[ {commander}||]' %
                                    len(articles))
        self.job.computer.db_session.commit()


class _GetUnreadRule(JobRule):
    spec = '[are there ]any news [on GalNet][ Astra]'

    def _process_recognition(self, node, extras):
        unread = self.job.get_unread_articles()
        if unread:
            num = 'is 1' if len(unread) == 1 else 'are %i' % len(unread)
            self.job.computer.io.notify('There %s new articles'
                                        '[ on GalNet|][ {commander}|]' % num)
            if YesNoJob.instance:
                self.job.computer.io.notify(
                    'Would you like to hear the titles?')
                YesNoJob.instance.init(self.job.read_titles(unread), None)
        else:
            self.job.computer.io.notify('There are no news[ {commander}|]')

class GnnewsReadLastRule(CommandRule):
    name = 'read last post'
    spec = '[please ]read me [[the] last] [<n>] [GalNet] post[s]'
    extras = [
        Number('n', (1, 10))
    ]

    def _process_recognition(self, node, extras):
        job = self.computer.get_job('GalnetJob')
        if job is None:
            self.computer.io.notify('GalNet job is not running')
            return
        num = extras.get('n', 1)
        posts = job.get_unread_articles()
        for post in posts[:num]:
            job.read_article(post)


class GnnewsReadThemRule(CommandRule):
    name = 'Read news'
    spec = '(read them [to me]|let me hear (it|them))'

    def _process_recognition(self, node, extras):
        job = self.computer.get_job('GalnetJob')
        if job is None:
            self.computer.io.notify('GalNet job is not running')
            return
        if job.awaiting_confirmation and \
                        job.awaiting_confirmation_until > time.time():
            job.read_news(job.awaiting_confirmation)
            job.awaiting_confirmation = False
        else:
            t = 'Please elaborate'
            self.computer.io.notify(t)