from abc import abstractproperty

from dragonfly import CompoundRule


__author__ = 'Lai'


class JobRule(CompoundRule):
    def __init__(self, job, *args, **kwargs):
        self.job = job
        super(JobRule, self).__init__(*args, **kwargs)


class ComputerJob(object):
    instance = None

    def __init__(self, computer):
        self.__class__.instance = self
        self.computer = computer

    def start(self):
        """ Subsystem initializer, should not use other subsystems.

        .. see also:
           :method:launch()
        """

        pass

    def launch(self):
        """ Called once before the main loop.

        .. see also:
           :method:`start`
        """
        pass

    def job(self):
        """ Called each iteration of the main loop """
        pass

    def stop(self):
        """ Called before computer is stopped.

        Should not reference any other jobs.
        """
        pass

    @abstractproperty
    def name(self):
        return "unnamed"