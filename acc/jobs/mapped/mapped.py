from dragonfly import MappingRule, Key

from acc.jobs.job import ComputerJob


class MappedJob(ComputerJob):
    def name(self):
        return "KeyMap"

    def start(self):
        class _Rule(MappingRule):
            mapping = {spec: Key(actions.get('keys', '')) for spec, actions in
                       self.computer.config.get("key_mapping", {}).iteritems()}

        self.computer.grammar.add_rule(_Rule())