import uuid
import datetime

from sqlalchemy import Column, DateTime, String

from acc.database import db_base
from acc.jobs.job import ComputerJob
from acc.jobs.remainder.rules import RemindmeRule


class Remainder(db_base):
    __tablename__ = "remainders"

    uid = Column(String, primary_key=True, unique=True)
    date = Column(DateTime)
    body = Column(String)


class RemainderJob(ComputerJob):
    def name(self):
        return "Remainders"

    def __init__(self, *args, **kwargs):
        super(RemainderJob, self).__init__(*args, **kwargs)

    def start(self):
        self.computer.grammar.add_rule(RemindmeRule(self.computer))

    def undo_remainder(self, uid):
        def _wr():
            self.computer.db_session.query(Remainder) \
                .filter(Remainder.uid == uid).delete('fetch')
            self.computer.db_session.commit()
        return _wr

    def add_remainder(self, date, body):
        astra_control = self.computer.get_job('AstracontrolJob')
        uid = str(uuid.uuid1())
        self.computer.db_session.add(Remainder(uid=uid, date=date, body=body))
        if astra_control:
            astra_control.add_history('Remainder %s' % body,
                                      self.undo_remainder(uid), uid)

    def job(self):
        astra_control = self.computer.get_job('AstracontrolJob')
        courtecy_job = self.computer.get_job('ChitchatJob')
        for remainder in self.computer.db_session.query(Remainder).all():
            if remainder.date <= datetime.datetime.now():
                text = remainder.body
                self.computer.io.notify("{commander}, you've asked me to "
                                     "remind you {text}", text=text)
                self.undo_remainder(remainder.uid)()
                if astra_control:
                    astra_control.remove_history(remainder.uid)
                if courtecy_job:
                    courtecy_job.expect_gratitude()


