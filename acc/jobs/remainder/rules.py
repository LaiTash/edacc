from datetime import datetime, timedelta

from dragonfly import Dictation, Alternative, Literal
from dragonfly.all import Number

from acc.computer import CommandRule


class RemindmeRule(CommandRule):
    name = 'remind me rule'
    spec = "([Astra ] in <n> <measure> " \
           "[please ]remind me <remainder>[ please]|" \
           "[Astra ][please ]remind me <remainder> in <n> <measure>)"

    extras = [
        Number('n'),
        Alternative(name='measure',
                    children=map(Literal, ['second', 'seconds',
                                           'minute', 'minutes',
                                           'hour', 'hours',
                                           'day', 'days'])),
        Dictation('remainder')
    ]

    def _process_recognition(self, node, extras):
        job = self.computer.get_job('RemainderJob')
        if not job:
            self.computer.io.notify('Remainder job is not running')
            return
        num = extras.get('n')
        measure = extras.get('measure').format()
        body = extras.get('remainder').format()
        if measure.startswith('second'):
            imeasure = 1
        elif measure.startswith('minute'):
            imeasure = 60
        elif measure.startswith('hour'):
            imeasure = 3600
        else:
            imeasure = 86400
        date = imeasure * num
        job.add_remainder(datetime.now() + timedelta(seconds=date), body)
        self.computer.io.notify('Will remind you %s in %s %s' %
                                (body, num, measure))
