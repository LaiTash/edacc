from sqlalchemy import Column, String, PickleType
from sqlalchemy.orm.exc import NoResultFound

from acc.database import db_base
from acc.jobs.subsystems._subsystem import Subsystem


class GlobalVariable(db_base):
    __tablename__ = 'globals'

    name = Column(String, primary_key=True, unique=True)
    value = Column(PickleType, nullable=True)

    def __repr__(self):
        return "<global %s = %s>" % (self.name, self.value)

class GlobSubsystem(Subsystem):
    def name(self):
        return "subsystem_glob"

    def launch(self):
        self.computer.io.notify('GLOB subsystem online.')

    def get(self, item, default=None):
        try:
            return self[item]
        except KeyError:
            return default

    def _fetch(self, item):
        return self.computer.db_session.query(GlobalVariable) \
            .filter(GlobalVariable.name == item).one()

    def __getitem__(self, item):
        try:
            return self._fetch(item).value
        except NoResultFound:
            return self.computer.config[item]

    def __setitem__(self, key, value):
        self.computer.log('GLOB: set `%s` to `%s`' % (key, value))
        try:
            self._fetch(key).value = value
        except NoResultFound:
            variable = GlobalVariable(name=key, value=value)
            self.computer.db_session.add(variable)
        self.computer.db_session.commit()