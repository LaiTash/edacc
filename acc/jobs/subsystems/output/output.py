from dragonfly import get_engine

from acc.jobs.subsystems._subsystem import Subsystem
from acc.jobs.subsystems.output.rules import _CallMeSomethingRule, \
    _CallMeMasterRule, _CallMeDoctorRule, _CallMeByNameRule
from acc.randstring import randomise_string


__author__ = 'Lai'


class OutputSubsystem(Subsystem):
    def name(self):
        return "subsystem_output"

    def __init__(self, computer):
        super(OutputSubsystem, self).__init__(computer)

    def start(self):
        self.notify('Output subsystem online.')
        self.computer.grammar.add_rule(_CallMeSomethingRule(self))
        self.computer.grammar.add_rule(_CallMeMasterRule(self))
        self.computer.grammar.add_rule(_CallMeDoctorRule(self))
        self.computer.grammar.add_rule(_CallMeByNameRule(self))

    def notify(self, message, **format):
        self.speak(self.randomise_string(message, format))

    def speak(self, message):
        self.computer.log("SPEAKING: '''%s'''" % message)
        get_engine().speak(message)

    def randomise_string(self, string_, format=None):
        commander_name = self.computer.glob.get('commander_name').lower()
        commander_name = 'Master' if commander_name == 'master' \
            else "[%s|commander]" % commander_name
        quickstrings = {
            'commander': commander_name
        }
        quickstrings.update(format or {})
        return randomise_string(string_.format(**quickstrings))