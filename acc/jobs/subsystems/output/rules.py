from dragonfly import Dictation

from acc.jobs.job import JobRule

class _CallMeDoctorRule(JobRule):
    spec = "call me Doctor[ Astra]"

    def _process_recognition(self, node, extras):
        self.job.notify('Doctor? Doctor who?')


class _CallMeByNameRule(JobRule):
    spec = "call me by [my ]name[ Astra]"

    def _process_recognition(self, node, extras):
        self.job.computer.glob['commander_name'] = self.job.computer. \
            config.get('commander_name', 'commander')
        self.job.notify('[alright|ok|as you [like|wish]][ {commander}|]')


class _CallMeMasterRule(JobRule):
    spec = "call me Master[ Astra]"

    def _process_recognition(self, node, extras):
        self.job.computer.glob['commander_name'] = 'Master'
        self.job.notify('[as you wish|yes][, |] {commander}')


class _CallMeSomethingRule(JobRule):
    spec = "call me <name>[ Astra]"

    extras = [
        Dictation('name')
    ]

    def confirm(self, name):
        def _wr():
            self.job.computer.glob['commander_name'] = name
            self.job.notify('[alright|ok|as you [like|wish]][ {commander}|]')

        return _wr

    def _process_recognition(self, node, extras):
        name = extras.get('name').format()
        yn = self.job.computer.jobs.get('dialog_YesNo')
        if yn:
            self.job.notify('I should call you "%s", is this correct?' % name)
            yn.init(self.confirm(name), None)
        else:
            self.confirm(name)()
