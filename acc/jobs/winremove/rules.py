from dragonfly import MappingRule, Dictation

NoAc = lambda: None


class WinRemoveRule(MappingRule):
    mapping = {
        'switch to <text>': NoAc,
        'system': NoAc,
        'New line': NoAc,
        'New paragraph': NoAc,
        'Tab': NoAc,
        'Literal <text>': NoAc,
        'Go to <text>': NoAc,
        'Go after <text>': NoAc,
        'No space': NoAc,
        'Go to start of sentence': NoAc,
        'Select all': NoAc,
        'Select that': NoAc,
        'Press <text>': NoAc,
        'Start': NoAc,
        'Apps': NoAc,
        'Right-click <text>': NoAc,
        'Left-click <text>': NoAc,
        'Click <text>': NoAc,
        'Double-click <text>': NoAc,
        'File': NoAc,
        'Show Desctop': NoAc,
        'Open <text>': NoAc,
        'Close that': NoAc,
        'Close <text>': NoAc,
        'Minimize': NoAc,
        'Maximize': NoAc,
        'Restore': NoAc,
        '<text>': NoAc,
    }

    extras = [
        Dictation('text')
    ]

