from acc.jobs.job import ComputerJob
from acc.jobs.winremove.rules import WinRemoveRule


class WinremoveJob(ComputerJob):
    def name(self):
        return "WinRemove"

    def start(self):
        self.computer.grammar.add_rule(WinRemoveRule())