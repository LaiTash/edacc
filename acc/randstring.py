from random import choice


def randomise_string(string_):
    result = ''
    choices_stack = [[]]
    strings_stack = ['']
    for c in string_:
        if c == '[':
            choices_stack.append([])
            strings_stack.append('')
        elif c in ('|]'):
            choices_stack[-1].append(strings_stack.pop())
            if c == ']':
                strings_stack[-1] += choice(choices_stack.pop())
            else:
                strings_stack.append('')
        else:
            strings_stack[-1] += c
    return strings_stack.pop()
