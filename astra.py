import json

from acc.computer import Computer
from acc.jobs.astracontrol.astracontrol import AstracontrolJob
from acc.jobs.chitchat.chitchat import ChitchatJob
from acc.jobs.dialogs.yesno.job import YesNoJob
from acc.jobs.galnet.gnnews import GalnetJob
from acc.jobs.mapped.mapped import MappedJob
from acc.jobs.remainder.remainder import RemainderJob
from acc.jobs.subsystems.glob.glob import GlobSubsystem
from acc.jobs.subsystems.output.output import OutputSubsystem
from acc.jobs.winremove.winremove import WinremoveJob


computer = Computer(json.load(open('config.json', 'r')))

computer.init_subsystems({
    'output': OutputSubsystem(computer),
    'glob': GlobSubsystem(computer)
})

jobs = {
    "AstraControl": AstracontrolJob,
    "Chitchat": ChitchatJob,
    "dialog_YesNo": YesNoJob,
    "GalNetNews": GalnetJob,
    "KeyMap": MappedJob,
    "Remainders": RemainderJob,
    "WinRemove": WinremoveJob,
}

for name, job in jobs.iteritems():
    if computer.config.get(name, True):
        print job
        computer.add_job(job(computer=computer))

computer.run()